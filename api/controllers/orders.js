const mongoose = require('mongoose');

const Order = require('../models/order');
const Product = require('../models/product');

exports.orders_get_all = (req, res, next) => {
    Order.find()
        .select('product quantity _id')
        .populate('product', '_id name')
        .exec()
        .then(result => {
            console.log(result);
            if (result) {
                res.status(200).json({
                    count: result.length,
                    orders: result.map(o => {
                        return {
                            _id: o._id,
                            product: o.product,
                            quantity: o.quantity,
                            request: {
                                type: 'GET',
                                url: 'http://localhost:3000/orders/' + o._id,
                                description: 'Get order details'
                            }
                        };
                    })
                });
            } else {
                res.status(404).json({ message: 'Found nothing' });
            }
        })
        .catch(e => {
            console.error(e);
            res.status(500).json({ error: e });
        });
};

exports.orders_create_order = (req, res, next) => {
    const proId = req.body.productId;
    Product.findById(proId)
        .select('product quantity _id')
        .exec()
        .then(pro => {
            if (!pro) {
                return res.status(404).json({
                    message: 'Product not found',
                    pro: pro,
                    id: proId
                });
            }
            const order = new Order({
                _id: mongoose.Types.ObjectId(),
                product: proId,
                quantity: req.body.quantity
            });

            console.log(order);
            return order.save();
        })
        .then(result => {
            console.log(result);
            res.status(201).json({
                message: 'Order stored',
                createdOrder: {
                    _id: result._id,
                    product: result.product,
                    quantity: result.quantity
                },
                request: {
                    type: 'GET',
                    url: 'http://localhost:3000/orders/' + result._id
                }
            });
        })
        .catch(e => {
            console.error(e);
            res.status(500).json({ error: e });
        });
};

exports.orders_get_single = (req, res, next) => {
    Order.findById(req.params.orderId)
        .select('product quantity _id')
        .exec()
        .then(order => {
            console.log(order);
            if (order) {
                res.status(200).json({
                    order: order,
                    request: {
                        type: 'GET',
                        url: 'http://localhost:3000/orders/',
                        description: 'Get all orders'
                    }
                });
            } else {
                res.status(404).json({ message: 'Order not found' });
            }
        })
        .catch(e => {
            console.error(e);
            res.status(500).json({ error: e });
        });
};

exports.orders_delete_order = (req, res, next) => {
    Order.findOneAndRemove({ _id: req.params.orderId })
        .select('product quantity _id')
        .exec()
        .then(result => {
            res.status(200).json({
                message: 'Order deleted',
                deletedOrder: result,
                request: {
                    type: 'POST',
                    url: 'http://localhost:3000/orders/',
                    body: {
                        productId: 'ID',
                        quantity: 'Number'
                    },
                    description: 'Create a new order'
                }
            });
        })
        .catch(e => {
            console.error(e);
            res.status(500).json({ error: e });
        });
};
