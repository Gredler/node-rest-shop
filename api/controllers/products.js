const Product = require('../models/product');
const mongoose = require('mongoose');

exports.products_get_all = (req, res, next) => {
    Product.find()
        .select('_id name price productImage')
        .exec()
        .then(result => {
            const response = {
                count: result.length,
                products: result.map(r => {
                    return {
                        _id: r._id,
                        name: r.name,
                        price: r.price,
                        productImage: r.productImage,
                        request: {
                            type: 'GET',
                            url: 'http://localhost:3000/products/' + r._id
                        }
                    };
                })
            };

            if (res) {
                res.status(200).json(response);
            } else {
                res.status(404).json({ message: 'No entries found' });
            }
        })
        .catch(e => {
            console.error(e);

            res.status(500).json({ error: e });
        });
};

exports.products_create_product = (req, res, next) => {
    console.log(req.file);
    const product = new Product({
        _id: new mongoose.Types.ObjectId(),
        name: req.body.name,
        price: req.body.price,
        productImage:
            'http://localhost:3000/' + req.file.path.replace('\\', '/')
    });

    product
        .save()
        .then(result => {
            console.log(result);

            res.status(201).json({
                message: 'Handling POST request to /products',
                product: {
                    _id: result._id,
                    name: result.name,
                    price: result.price,
                    productImage: result.productImage,
                    request: {
                        type: 'GET',
                        url: 'http://localhost:3000/products/' + result._id
                    }
                }
            });
        })
        .catch(e => {
            console.error(e);

            res.status(500).json({
                error: e
            });
        });
};

exports.products_get_single = (req, res, next) => {
    const id = req.params.productId;
    Product.findById(id)
        .select('_id name price productImage')
        .exec()
        .then(result => {
            console.log(result);

            if (result) {
                res.status(200).json({
                    product: result,
                    request: {
                        type: 'GET',
                        url: 'http://localhost:3000/products/',
                        description: 'Get all products'
                    }
                });
            } else {
                res.status(404).json({ message: 'No valid entry found.' });
            }
        })
        .catch(e => {
            console.error(e);
            res.status(500).json({ error: e });
        });
};

exports.products_patch_product = (req, res, next) => {
    const id = req.params.productId;
    const updateOps = {};

    for (const ops of req.body) {
        updateOps[ops.propName] = ops.value;
    }

    Product.update({ _id: id }, { $set: updateOps })
        .exec()
        .then(result => {
            console.log(result);

            res.status(200).json({
                message: 'Product updated',
                request: {
                    type: 'GET',
                    url: 'http://localhost:3000/products/' + id
                }
            });
        })
        .catch(e => {
            console.error(e);

            res.status(500).json({ error: e });
        });
};

exports.products_delete_product = (req, res, next) => {
    const id = req.params.productId;

    // findOneAndRemove instead of just delete because I'm using MongoDB Version 3.6+
    Product.findOneAndRemove({ _id: id })
        .select('_id name price productImage')
        .exec()
        .then(result => {
            res.status(200).json({
                deleted_object: result,
                request: {
                    type: 'POST',
                    url: 'http://localhost:3000/products/',
                    body: {
                        name: 'String',
                        price: 'Number'
                    },
                    description: 'Create new Products'
                }
            });
        })
        .catch(e => {
            console.error(e);
            res.status(500).json({ error: e });
        });
};
