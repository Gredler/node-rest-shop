const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const User = require('../models/user');

exports.user_signup = (req, res, next) => {
    User.find({ email: req.body.email })
        .exec()
        .then(u => {
            if (u.length >= 1) {
                return res
                    .status(409)
                    .json({ message: 'Email already exists' });
            } else {
                bcrypt.hash(req.body.password, 10, (err, hash) => {
                    if (err) {
                        return res.status(500).json({
                            error: err
                        });
                    } else {
                        const user = new User({
                            _id: new mongoose.Types.ObjectId(),
                            email: req.body.email,
                            password: hash
                        });

                        user.save()
                            .then(result => {
                                console.log(user);
                                res.status(201).json({
                                    message: 'User created',
                                    user: user
                                });
                            })
                            .catch(e => {
                                console.error(e);
                                res.status(500).json({ error: e });
                            });
                    }
                });
            }
        })
        .catch(e => {
            console.error(e);
            res.status(500).json({ error: e });
        });
};

exports.user_login = (req, res, next) => {
    User.findOne({ email: req.body.email })
        .exec()
        .then(user => {
            if (!user) {
                return res.status(401).json({ message: 'Auth failed' });
            }

            bcrypt.compare(
                req.body.password,
                user.password,
                (err, response) => {
                    if (err) {
                        return res.status(401).json({ message: 'Auth failed' });
                    }
                    if (response) {
                        const token = jwt.sign(
                            {
                                email: user.email,
                                userId: user._id
                            },
                            process.env.JWT_KEY,
                            {
                                expiresIn: '2h'
                            }
                        );
                        return res.status(200).json({
                            message: 'Auth successful',
                            token: token
                        });
                    }

                    res.status(401).json({ message: 'Auth failed' });
                }
            );
        })
        .catch(e => {
            console.error(e);
            res.status(500).json({ error: e });
        });
};

exports.user_delete_user = (req, res, next) => {
    User.findByIdAndRemove({ _id: req.params.userId })
        .select('_id email password')
        .exec()
        .then(result => {
            if (result) {
                res.status(200).json({
                    message: 'User delete',
                    deletedUser: result
                });
            } else {
                res.status(404).json({ message: 'User not found' });
            }
        })
        .catch(e => {
            console.error(e);
            res.status(500).json({ error: e });
        });
};
